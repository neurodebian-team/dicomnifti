#ifndef DICOMHEAD_H_INCLUDED
#define DICOMHEAD_H_INCLUDED
//  $Id: dicomhead.h,v 1.2 2006-08-15 20:02:29 valerio Exp $	

//****************************************************************************
//
// Modification History (most recent first)
// mm/dd/yy  Who  What
//
// 08/15/06  VPL  GPL'ed it
// 11/28/05  VPL  
//
//****************************************************************************

#include <sys/types.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <iomanip>

using namespace std;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::setw;
using std::ofstream;
using std::ios;
using std::string;

#endif
