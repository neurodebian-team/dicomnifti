#ifndef NIFTIOUT_H_INCLUDED
#define NIFTIOUT_H_INCLUDED
//  $Id: niftiout.h,v 1.3 2006-08-15 20:02:29 valerio Exp $	

//============================================================================
// CTN software is curtesy of
//
//		Mallinckrodt Institute of Radiology
//		Washington University in St. Louis MO
//		http://wuerlim.wustl.edu/
//
//============================================================================

//****************************************************************************
//
// Modification History (most recent first)
// mm/dd/yy  Who  What
//
// 08/15/06  VPL  GPL'ed it
// 03/22/05  VPL  Allow processing of multiple series
//                Add NYU License agreement
// 02/01/05  VPL  
//
//****************************************************************************

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <list>

using namespace std;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::ios;
using std::string;

typedef string::size_type size_type;

#include "dinifti.h"
#include "dicomInfo.h"
#include "nifti1_io.h"

class DICOMImage;

bool NIfTICreateHeader(nifti_image &hdr, IMAGEMAP &imageMap);
bool NIfTIWriteData(znzFile fp, nifti_image &hdr, IMAGEMAP &imageMap);

#endif
