#ifndef DINIFTI_H_INCLUDED
#define DINIFTI_H_INCLUDED
//  $Id: dinifti.h,v 1.11 2007-10-15 17:16:28 valerio Exp $	

//============================================================================
// CTN software is curtesy of
//
//		Mallinckrodt Institute of Radiology
//		Washington University in St. Louis MO
//		http://wuerlim.wustl.edu/
//
//============================================================================

//****************************************************************************
//
// Modification History (most recent first)
// mm/dd/yy  Who  What
//
// 10/04/06  Add DiniftiOptions class and verbose mode
//           (contributed by Michael Hanke)
// 08/15/06  VPL  GPL'ed it
// 03/22/05  VPL  Allow processing of multiple series
//                Add NYU License agreement
// 01/24/05  VPL  
//
//****************************************************************************

#include "dicomInfo.h"

#define OUT_BUFFER_SIZE 1024*1024

// Need to do some prototyping to help us
class DiniftiOptions;

enum OUTPUT_TYPE { ANALYZE, NIfTI_SINGLE, NIfTI_DUAL, NIfTI_ASCII };

void StoreImageFiles(std::vector<std::string> &dicomFiles, SERIESMAP &seriesMap, char *outName, const DiniftiOptions& opts);

void ProcessSeries(SeriesInfo &seriesInfo, const DiniftiOptions& opts);

class DiniftiOptions
{
public:
	DiniftiOptions()
		:useSerDesc(false),
		 nameFormat(NULL),
		 verbose(false),
		 noact(false),
		 numSlices(0),
		 compressed(false),
		 niftiFormat(NIfTI_SINGLE)
	{}
	bool useSerDesc;
	char *nameFormat;
	bool verbose;
	bool noact;
	int numSlices;
	bool compressed;
	OUTPUT_TYPE niftiFormat;
};


#endif
