Requirements
============

   The program depends on the CTN library developed by the
Mallinckrodt Institue of Radiology, Washington University in St. Louis
MO, http://wuerlim.wustl.edu/. The source code can be downloaded from
ftp://ftp.erl.wustl.edu/pub/dicom/software/ctn/  

   The program also depends on the nifticlib developed by the NIfTI
project, http://nifti.nimh.nih.gov/. The source code can be downloaded
from http://sourceforge.net/projects/niftilib. 

Basic Installation
==================

   Using cmake:
   ============

   http://cmake.org/

   1. `cd' to the `build' subdirectory of the program's top level
      directory:
	cd $DINIFTI_ROOT/build

   2. Type `cmake ..' to configure the package for your system. While
      running, cmake prints some messages telling which features it is
      checking for and what is missing.

   3. Type `make all' to compile the package.

   4. Type `make install' to install the programs and any data files and
      documentation.

   5. You can remove the program binaries and object files from the
      source code directory by typing `make clean'.  


   USING GNU autoconf/configure:
   =============================

   The file `configure.in' is used to create `configure' by a program
   called `autoconf'.  You will need to run `autoconf' at least once to
   generate the correct `configure' shell script. 

   The `configure' shell script attempts to guess correct values for
   various system-dependent variables used during compilation.  It uses
   those values to create a `Makefile' in each directory of the package.
   It may also create one or more `.h' files containing system-dependent
   definitions.  Finally, it creates a shell script `config.status' that
   you can run in the future to recreate the current configuration, a file
   `config.cache' that saves the results of its tests to speed up
   reconfiguring, and a file `config.log' containing compiler output
   (useful mainly for debugging `configure').

   1. `cd' to the program's top level directory and type
      `autoconf'. You will need to do this only the first time you
      build, or if you are building on different architectures.

   2. Type `./configure' to configure the package for your system.  If
      you're using `csh' on an old version of System V, you might need
      to type `sh ./configure' instead to prevent `csh' from trying to
      execute `configure' itself.

      Running `configure' takes awhile.  While running, it prints some
      messages telling which features it is checking for.

   3. Type `make all' to compile the package.

   4. Type `make install' to install the programs and any data files and
      documentation.

   5. You can remove the program binaries and object files from the
      source code directory by typing `make clean'.  
   

Installation Names
==================

   By default, `make install' will install the package's files in
   `/usr/local/bin', `/usr/local/man', etc.  You can specify an
   installation prefix other than `/usr/local' by:

   USING cmake:
   ============ 
 
   specifying it when running `cmake':
	cmake .. -DPREFIX:PATH=<my top level>

   You can also specify separate include, lib and bin locations:
	cmake .. -DINCLUDEDIR:PATH=<my includes>
	cmake .. -DLIBDIR:PATH=<my libraries>
	cmake .. -DBINDIR:PATH=<my executables>

   All parameters can also be modified using `ccmake ..'.

   
   USING GNU autoconf/configure:
   =============================

   by giving `configure' the option `--prefix=PATH'.

   You can specify separate installation prefixes for
   architecture-specific files and architecture-independent files.  If you
   give `configure' the option `--exec-prefix=PATH', the package will use
   PATH as the prefix for installing programs and libraries.
   Documentation and other data files will still use the regular prefix.

   In addition, if you use an unusual directory layout you can give
   options like `--bindir=PATH' to specify different values for particular
   kinds of files.  Run `configure --help' for a list of the directories
   you can set and what kinds of files go in them.

